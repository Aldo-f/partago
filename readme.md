# Partago live kaart

See live on [partago.project.aldofieuw.com](http://partago.project.aldofieuw.com)<br/>
With data from [Open Data Portaal](https://data.stad.gent/data/635) 

More info about this project?
* http://aldofieuw.com/project/partago-deelwagens 