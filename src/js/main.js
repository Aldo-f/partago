/* eslint-disable no-console */


/* Global vars
 ***************/
var sLink = "https://datatank.stad.gent/4/mobiliteit/deelwagenspartago.json";
var sData = "";
var oData = {};
var bToonKaart = false;
// Json
var displayName = "";
var stationType = "";
var geoPosition = {};
var longitude = "";
var latitude = "";
var vehicleInformation = {};
var fuelType = "";
var transmissionType = "";
var GeoJSON = {
    type: "FeatureCollection",
    features: []
}
var features = [];
var map;

window.onload = function () {
    // alert("main.js geladen.");

    // Elements    
    var eOutput = document.getElementById("output");
    var eBtn = document.getElementById("btn-collapse")
    eOutput.innerHTML = "";


    haalJSONop(sLink, sData);
    toonData(sData, eOutput);


    // Switch tekst op button
    eBtn.addEventListener("click", function () {
        if (eBtn.innerHTML == "Toon kaart") {
            eBtn.innerHTML = "Toon tabel";
        } else {
            eBtn.innerHTML = "Toon kaart";
        }
        bToonKaart != bToonKaart
        console.log("Geklikt");
        setTimeout(function () {
            map.resize();
        }, 10);
    });
}

function haalJSONop(sLink) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            sData = this.responseText;
            // console.log(sData);
        }
    };
    // xhttp.open("GET", "src/assets/response.json", true);
    xhttp.open("GET", sLink, false);
    xhttp.send();
}

function toonData(sData, eOutput) {

    maakMap();


    // Parse van string naar object
    oData = JSON.parse(sData);

    // DEBUG: al de keys
    // let keys = Object.keys(oData);
    // console.log(keys);

    // Toon heading 
    var eDiv = document.createElement("div");
    eDiv.setAttribute("class", "table-responsive");
    var eTable = document.createElement("table");
    eTable.setAttribute("class", "table table-inverse");
    var eThead = document.createElement("thead");
    eThead.setAttribute("class", "thead-default");
    var eRow = document.createElement("tr");
    var eTh = document.createElement("th");

    // maak clones
    var cTh = eTh.cloneNode(false);
    cTh.innerHTML = "Naam auto";
    eRow.appendChild(cTh);

    cTh = eTh.cloneNode(false);
    cTh.innerHTML = "logitude";
    eRow.appendChild(cTh);

    cTh = cTh.cloneNode(false);
    cTh.innerHTML = "latitude";
    eRow.appendChild(cTh);

    cTh = cTh.cloneNode(false);
    cTh.innerHTML = "model";
    eRow.appendChild(cTh);

    cTh = cTh.cloneNode(false);
    cTh.innerHTML = "brand";
    eRow.appendChild(cTh);

    eThead.appendChild(eRow);
    eTable.appendChild(eThead);

    var eTbody = document.createElement("tbody");

    for (const item of oData) {
        displayName = item.displayName;
        stationType = item.stationType;
        geoPosition = item.geoPosition; // object
        latitude = item.geoPosition.latitude;
        longitude = item.geoPosition.longitude;
        vehicleInformation = item.vehicleInformation; // object
        fuelType = item.vehicleInformation.fuelType;
        transmissionType = item.vehicleInformation.transmissionType;
        let model = item.vehicleInformation.model ? item.vehicleInformation.model : "";
        let brand = item.vehicleInformation.brand ? item.vehicleInformation.brand : "";

        // Toon de opgehaalde data
        // var eDiv = document.createElement('div');
        // eDiv.innerHTML = "De auto genaamd " + displayName + " staat op latitude " + latitude + " en op logitude " + longitude + "<br>";
        // eOutput.appendChild(eDiv);

        /* Maak een geoJSON object aan
         ******************/
        GeoJSON.features = voegAutoToeInGeoJSON(displayName, latitude, longitude);


        /* Toon als tabel
         ******************/
        var cRow = eRow.cloneNode(false);
        var eTd = document.createElement("td");

        eTd.innerHTML = displayName;
        cRow.appendChild(eTd);

        var cTd = eTd.cloneNode(false);
        cTd.innerHTML = longitude.toFixed(4);
        cRow.appendChild(cTd);

        cTd = eTd.cloneNode(false);
        cTd.innerHTML = latitude.toFixed(4);
        cRow.appendChild(cTd);

        cTd = eTd.cloneNode(false);
        cTd.innerHTML = model;
        cRow.appendChild(cTd);

        cTd = eTd.cloneNode(false);
        cTd.innerHTML = brand;
        cRow.appendChild(cTd);

        // Row aan body
        eTbody.appendChild(cRow);
    }

    eTable.appendChild(eTbody);
    eDiv.append(eTable);
    eOutput.appendChild(eDiv);

    // console.log(GeoJSON);
    // add markers to map

    // Plaats markers op kaart
    GeoJSON.features.forEach(function (marker) {

        // create a HTML element for each feature
        var el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add to the map
        new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(new mapboxgl.Popup({
                    offset: 25
                }) // add popups
                .setHTML('<div>' + marker.properties.title + '</div>'))

            .addTo(map);
    });


}

function maakMap() {
    // https://docs.mapbox.com/help/tutorials/custom-markers-gl-js/
    // console.log("in ToonMap");

    mapboxgl.accessToken = 'pk.eyJ1IjoiYWxkby1mIiwiYSI6ImNqcGNxb2V1aTBoZ2YzamxrNHBoam5xYXAifQ.P88Tv4Zu-8vDFL2h62YdSA';
    map = new mapboxgl.Map({
        container: 'kaart',
        style: 'mapbox://styles/aldo-f/cju5nyj180x7g1fmj6yc1k2lp',
        center: [4.1128894, 51.0224673],
        zoom: 9.0
    });

    map.on('load', function () {
        var mapCanvas = document.getElementsByClassName('mapboxgl-canvas')[0];
        mapCanvas.style.width = '100%';
    });
}

function voegAutoToeInGeoJSON(displayName, latitude, logitude) {

    let auto = {
        type: "Feature",
        properties: {
            title: displayName
        },
        geometry: {
            type: "Point",
            coordinates: [
                logitude,
                latitude
            ]
        }
    };

    features.push(auto);
    return features;



}

(function () {
    var ref = window.document.getElementsByTagName('script')[0];
    var script = window.document.createElement('script');
    script.src = 'node_modules/mapbox-gl/dist/mapbox-gl.js';
    ref.parentNode.insertBefore(script, ref);
})();